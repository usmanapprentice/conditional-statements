function FindRoot (array) {
	let a = +(array[0]),
		b = +(array[1]),
		c = +(array[2]);
	let X1,	X2;
	let D = Math.pow(b,2) - (4*a*c);
	if (D < 0) {
		console.log(' No real roots');
	}else if (D === 0) {
		X1 = -b/(2*a);
		X1 = X1.toFixed(2);
		X2 = X1;
		console.log(`X1 = X2 = ${X1}`);
	}else if (D > 0) {
		X1 = (-b + Math.sqrt(D))/(2*a);
		X1 = X1.toFixed(2);
		X2 = (-b - Math.sqrt(D))/(2*a);
		X2 = X2.toFixed(2);
		console.log(`X1 = ${X1} ; X2 = ${X2}`);
	}
}

FindRoot(['0.2', '9.572', '0.2']);